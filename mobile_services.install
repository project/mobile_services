<?php
// $Id: mobile_services.install,v

/**
 * Implementation of hook_enable().
 */
function mobile_services_enable() {
  drupal_set_message(t('Mobile Services module successfully installed.'));
}

/**
 * Implementation of hook_install().
 */
function mobile_services_install() {
  // So our form alter will run before the other forms
  db_query("UPDATE {system} SET weight = %d WHERE name = '%s'", 100000, 'mobile_services');
  drupal_install_schema('mobile_services');
}

/**
 * Implementation of hook_uninstall().
 */
function mobile_services_uninstall() {
  drupal_uninstall_schema('mobile_services');
  variable_del('mobile_services_allowed_content_types');
  variable_del('mobile_services_allowed_views');
}

/**
 * Implementation of hook_schema()
 */
function mobile_services_schema() {
  return array(
    'mobile_services_view_fields_info' => array(
      'description' => t('Stores information pertaining to views, specifically view fields in the system'),
      'fields' => array(
        'field_id' => array(
          'description' => t('Primary key: Field ID'),
          'type' => 'serial',
          'not null' => TRUE
        ),
        'view' => array(
          'description' => t('Name for the view'),
          'type' => 'varchar',
          'length' => 1024,
          'not null' => TRUE
        ),
        'display_id' => array(
          'description' => t('Display ID of the view'),
          'type' => 'varchar',
          'length' => 1024,
          'not null' => TRUE
        ),
        'property' => array(
          'description' => t('Property we are setting for the field'),
          'type' => 'varchar',
          'length' => 1024,
          'not null' => TRUE
        ),
        'field_name' => array(
          'description' => t('Name of the field we are storing information for'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE
        ),
        'field_value' => array(
          'description' => t('Value for the information we wish to store'),
          'type' => 'text',
          'not null' => FALSE,
          'size' => 'big'
        ),
      ),
      'primary key' => array('field_id'),
    ),
    'mobile_services_content_fields_info' => array(
      'description' => t('Stores information'),
      'fields' => array(
        'field_id' => array(
          'description' => t('Primary key: Field ID'),
          'type' => 'serial',
          'not null' => TRUE,
        ),
        'content_type' => array(
          'description' => t('Content Type the field is associated with'),
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
        ),
        'field_name' => array(
          'description' => t('Name of the field we are storing information for'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
        'screen_position' => array(
          'description' => t('Position of the field on the mobile screen'),
          'type' => 'varchar',
          'length' => 32,
          'not null' => FALSE,
          'default' => 'head',
        ),
        'precedence' => array(
          'description' => t('Precedence the field takes in the screen position'),
          'type' => 'int',
          'not null' => FALSE,
          'default' => 0,
        ),
        'hide_display' => array(
          'description' => t('Boolean to determine if the field is displayed or hidden'),
          'type' => 'int',
          'not null' => FALSE,
          'default' => 0,
        ),
        'label' => array(
          'description' => t('Label for the field to display on the mobile screen'),
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ),
        'label_position' => array(
          'description' => t('Position of the label on the screen in relation to the field'),
          'type' => 'varchar',
          'length' => 32,
          'not null' => FALSE,
          'default' => 'left',
        ),
        'hide_label' => array(
          'description' => t('Boolean to determine whether the label should be shown or not'),
          'type' => 'int',
          'not null' => FALSE,
          'default' => 1,
        ),
        'datatype' => array(
          'description' => t('Type of data that shall be delivered by the field'),
          'type' => 'varchar',
          'length' => 32,
          'not null' => FALSE,
          'default' => 'string',
        ),
        'highlight' => array(
          'description' => t('Determine whether the field will be highlighted / bolded on the mobile screen'),
          'type' => 'int',
          'not null' => FALSE,
          'default' => 0,
        ),
      ),
      'primary key' => array('field_id'),
      'unique keys' => array(
        'content_type_field_name' => array('content_type', 'field_name')
      ),
    ),
  );
}
