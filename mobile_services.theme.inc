<?php
// $id

/**
 * Theme the content types allowed content types form
 */
function theme_mobile_services_content_types_allowed_content_types_form($form) {
  if (isset($form['allowed_content_types'])) {
    $output = drupal_render($form['allowed_content_types']);
  }
  $rows = array();
  $rows[] = array(
    'content_type' => drupal_render($form['content_type']),
    'link' => drupal_render($form['submit']),
  );
  $output .= theme('table', NULL, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Theme field mappings for the content types
 */
function theme_mobile_services_content_types_field_mappings_form($form) {
  if (isset($form['mappings'])) {
    $output = drupal_render($form['mappings']);
  }
  $row = array();
  $rows[] = array(
    'fields' => drupal_render($form['fields']),
    'link' => drupal_render($form['submit']),
  );
  $output .= theme('table', NULL, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Theme the views allowed views form
 */
function theme_mobile_services_views_allowed_views_form($form) {
  if (isset($form['allowed_views'])) {
    $output = drupal_render($form['allowed_views']);
  }
  $rows = array();
  $rows[] = array(
    'view' => drupal_render($form['views']),
    'link' => drupal_render($form['submit']),
  );
  $output .= theme('table', NULL, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Theme the views edit form
 */
function theme_mobile_services_views_edit_form($form) {
  $output = drupal_render($form);
  return $output;
}
