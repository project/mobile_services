<?php

include_once './' . drupal_get_path('module', 'views') . '/plugins/views_plugin_row_fields.inc';

class mobile_services_row_style_plugin extends views_plugin_row_fields {
  function render($row, $strip_html = FALSE) {
    $result = array();

    foreach ($this->view->field as $field => $field_conf) {
      if (is_callable(array($field_conf, 'theme')) || is_callable(array($field_conf, 'render'))) {
        if (method_exists($field_conf, 'theme')) {
          $result[$field] = $field_conf->theme($row);
        } else {
          $result[$field] = $field_conf->render($row);
        }

        if ($strip_html) {
          $result[$field] = strip_tags(trim($result[$field]));
        }
      }
    }

    return $result;
  }
}
