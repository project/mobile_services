<?php

/**
 * Function returns all pertinent information for a node based on the node id
 */
function mobile_services_node_get($nid = NULL, $viewed = FALSE) {
  if (is_null($nid) || !is_numeric($nid)) {
    return NULL;
  }
  // Retrieve the type for the node we have been requested
  $allowed_content_types = variable_get('mobile_services_allowed_content_types', array());
  $node = array();
  $node_info = node_load(array('nid' => $nid));
  // If not set then we are not allowing this type through the mobile services
  if ($node_info && isset($allowed_content_types[$node_info->type])) {
    $cfields = mobile_services_content_type_get_configured_fields($node_info->type);
    $node['field_info'] = $cfields;
    $fields = array_merge(array('nid', 'type', 'uid', 'status', 'created', 'comment', 'body', 'title'), array_keys($cfields));
    $fields = mobile_services_node_verify_fields($fields);
    $node_info = services_node_load($node_info, $fields);
    $node_array = array();
    foreach ($node_info as $key => $value) {
      if (is_array($value) || is_object($value)) {
        $node_array[$key] = (string)mobile_services_node_special_fields_handling_value($key, $value);
      }
      else {
        $node_array[$key] = (string)$value;
      }
    }
    $node_array['comment_count'] = "0";
    // Determine if there are actually any comments for this node at all
    if (isset($node_array['comment']) && is_numeric($node_array['comment']) && $node_array['comment'] == 2) {
      $node_array['comment_count'] = (string)comment_num_all($node_array['nid']);
    }
    $node['node'] = $node_array;
    // Update viewed for this user
    if ($viewed === TRUE) {
      node_tag_new($nid);
    }
  }
  return $node;
}

/**
 * Function to save the node in drupal and do relevant processing
 * On the Drupal side rather than overloading the mobile client
 *
 * @param $node
 *  Object holding all of the pertinent information regarding the node
 */
function mobile_services_node_save($node = array()) {
  module_load_include('inc', 'mobile_services', 'mobile_services.content');
  $form_fields = $node_values = array();
  $node_type = isset($node['type']) ? $node['type'] : NULL;
  if ($node_type) {
    $form_data = mobile_services_content_types_describe($node_type);
    $form_fields = $form_data['form_fields'];
    foreach ($form_fields as $field => $fieldvalue) {
      $name = $fieldvalue['#name'];
      if (!isset($node[$name])) { continue; } // not set so we don't try to access it
      $value = $fieldvalue['#value'];
      if (is_array($value) || (strpos($name, '[') && strpos($name, ']'))) {
        if (strpos($name, '[') && strpos($name, ']')) {
          $array_name = trim(str_replace(array('][', '[', ']'), ' ', $name));
          $pieces = array_reverse(explode(" ", $array_name));
          $field_array = array();
          foreach ($pieces as $key) {
            if (empty($field_array)) {
              $holder = array($key => $node[$name]);
            }
            else {
              $holder = array($key => $field_array);
            }
            $field_array = $holder;
          }
          $node_values += $field_array;
        }
        else {
          $node_values[$name] = array($node[$name] => $node[$name]);
        }
      }
      else {
        $node_values[$name] = $node[$name];
      }
    }
  }
  // Grabbing fields that should always be there but are not in the form fields configuration
  foreach (array('uid', 'name', 'type', 'status', 'comment') as $key) {
    if (!isset($node_values[$key]) && isset($node[$key])) {
      $node_values[$key] = $node[$key];
    }
  }
  $node = (object)$node_values;
  $node = node_submit($node);
  node_save($node);
  if (is_numeric($node->nid) && $node->nid > 0) {
    return (int)$node->nid;
  }
  return -1;
}

/**
 * Mobile Services function utilized to retrieve the comments from a node
 */
function mobile_services_node_comments_get($nid = NULL) {
  $comment_info = $field_info = $comments = array();
  $result = db_query("SELECT cid FROM {comments} WHERE nid = %d ORDER BY thread DESC", $nid);
  while ($comment = db_fetch_array($result)) {
    // Make sure we are getting an array back instead of object
    $comment = (array)_comment_load($comment['cid']);
    foreach ($comment as $field => $value) {
      // Make sure the value are strings so processing on app side is easier
      $comment[$field] = (string)$value;
    }
    $comments[] = $comment;
  }
  // Only provide information back if we actually had comments
  if (!empty($comments)) {
    $comment_info['fields'] = mobile_services_node_comment_fields();
    $comment_info['comments'] = $comments;
  }
  return $comment_info;
}

/**
 * Function utilized too save the comment to mobile services
 */
function mobile_services_comment_save($comment = NULL) {
  // If the submitted comment does not contain a uid, set it to anonymous.
  if (isset($comment->uid)) {
    $uid = 0;
  }

  // If the submitted comment does not contain a nid, then return an error.
  if (!isset($comment->nid)) {
    return services_error(t("No node specified."));
  }

  // If a cid is present then the user is trying to edit an existing comment.
  if (isset($comment->cid) && ($comment->cid != 0)) {
    $initial_comment = _comment_load($comment->cid);
    $admin = user_access("administer comments");

    if ($initial_comment->uid == 0 && !$admin) {
      return services_error(t("Anonymous comments can't be edited"));
    }

    if (($initial_comment->uid != $comment->uid || comment_num_replies($comment->cid) != 0) && !$admin) {
      return services_error(t("User does not have permission to edit this comment"));
    }
  }

  // Can I just make a note here about how stupid it is that comment_load returns
  // an object but comment_save expects an array?
  return comment_save((array) $comment);
}

/**
 * Helper function to make sure we get the correct information regarding the fields
 */
function mobile_services_node_verify_fields($fields = array()) {
  $return_fields = array();
  foreach ($fields as $field) {
    if (in_array($field, array('body_field', 'og_nodeapi'))) {
      $field = mobile_services_node_special_field($field, $return_fields);
    }
    $return_fields[] = $field;
  }
  return array_unique($return_fields);
}

function mobile_services_node_special_field($field = NULL, &$return_fields = array()) {
  if (!is_null($field)) {
    switch ($field) {
      case 'body_field':
        $field = 'body';
        break;
      case 'og_nodeapi':
        $field = 'og_groups_both';
        break;
    }
  }
  return $field;
}

/**
 * Helper function to retrieve the field value as a string
 */
function mobile_services_node_special_fields_handling_value($key = NULL, $value = NULL) {
  $data = '';
  if ($key && $value) {
    switch ($key) {
      case 'og_groups_both':
        foreach ($value as $k => $v) {
          $data .= $v .'|'; // group names only
        }
        $data = trim($data, '|');
        break;
      case 'taxonomy':
        foreach ($value as $k => $v) {
          if (is_object($v)) {
            $data .= $v->name .',';
          }
        }
        $data = trim($data, ',');
        break;
      case 'locations':
        if (is_array($value) && isset($value[0])) {
          $value = $value[0];
        }
      case 'location':
        if (is_array($value)) {
          if (isset($value['street']) && drupal_strlen($value['street']) &&
              isset($value['city']) && drupal_strlen($value['city']) &&
              isset($value['province']) && drupal_strlen($value['province']) &&
              isset($value['country_name']) && drupal_strlen($value['country_name'])) {
            $data = $value['street'] .' '. $value['city'] .' '. $value['province'] .' '. $value['country_name'];
          }
          elseif (isset($value['latitude']) && drupal_strlen($value['latitude']) &&
                  isset($value['longitude']) && drupal_strlen($value['longitude'])) {
            $data = $value['latitude'] .' / '. $value['longitude'];
          }
        }
        break;
      default:
        // Most standard CCK type field that has been come across in the system
        if (is_array($value) && isset($value[0]) && is_array($value[0]) && isset($value[0]['value'])) {
          $data = $value[0]['value'];
        }
    }
    // Last resort allow any module to give a value if they implement the hook
    foreach (module_implements('mobile_services_field_value') as $module) {
      $data .= module_invoke($module, 'mobile_services_field_value', $key, $value);
    }
  }
  return $data;
}

/**
 * Function to describe the fields in comments
 */
function mobile_services_node_comment_fields() {
  $fields = array();
  $fields[] = array(
    'datatype' => 'int',
    'alias' => 'cid',
    'label' => t('Comment ID'),
    'hide_display' => "1",
    'label_position' => 'left',
    'hide_label' => "0",
    'highlight' => "0",
    'position' => 'above',
  );
  $fields[] = array(
    'datatype' => 'int',
    'alias' => 'pid',
    'label' => t('Parent ID'),
    'hide_display' => "1",
    'label_position' => 'left',
    'hide_label' => "0",
    'highlight' => "0",
    'position' => 'above',
  );
  $fields[] = array(
    'datatype' => 'int',
    'alias' => 'nid',
    'label' => t('Node ID'),
    'hide_display' => "1",
    'label_position' => 'left',
    'hide_label' => "0",
    'highlight' => "0",
    'position' => 'above',
  );
  $fields[] = array(
    'datatype' => 'int',
    'alias' => 'uid',
    'label' => t('UID'),
    'hide_display' => "1",
    'label_position' => 'left',
    'hide_label' => "0",
    'highlight' => "0",
    'position' => 'above',
  );
  $fields[] = array(
    'datatype' => 'string',
    'alias' => 'subject',
    'label' => t('Subject'),
    'hide_display' => "0",
    'label_position' => 'left',
    'hide_label' => "1",
    'highlight' => "1",
    'position' => 'above',
  );
  $fields[] = array(
    'datatype' => 'string',
    'alias' => 'name',
    'label' => t('Posted By'),
    'hide_display' => "0",
    'label_position' => 'left',
    'hide_label' => "1",
    'highlight' => "0",
    'position' => 'above',
  );
  $fields[] = array(
    'datatype' => 'date',
    'alias' => 'timestamp',
    'label' => t('Submitted'),
    'hide_display' => "0",
    'label_position' => 'left',
    'hide_label' => "1",
    'highlight' => "0",
    'position' => 'above',
  );
  $fields[] = array(
    'datatype' => 'string',
    'alias' => 'comment',
    'label' => t('Comment'),
    'hide_display' => "0",
    'label_position' => 'left',
    'hide_label' => "1",
    'highlight' => "0",
    'position' => 'below',
  );
  return $fields;
}
