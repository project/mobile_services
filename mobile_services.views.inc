<?php

/**
 * Function returns a list of the views supported for mobile development
 */
function mobile_services_views_list() {
  $views = array();
  $allowed_views = variable_get('mobile_services_allowed_views', array());
  foreach ($allowed_views as $view => $info) {
    $views[$view]['view_name'] = $view;
    $views[$view]['display_id'] = $info['display_id'];
    $views[$view]['display_name'] = $info['display_name'];
    // Retrieving the type of view that we have saves doing a complete view describe for each view
    $drupal_view = views_get_view($view);
    $drupal_view->set_display($info['display_id']);
    $drupal_view->build();
    $views[$view]['viewtype'] = empty($drupal_view->display_handler->options['style_options']['viewtype']) ?
                                'default' : $drupal_view->display_handler->options['style_options']['viewtype'];
    $views[$view]['title'] = empty($drupal_view->display_handler->options['style_options']['title']) ?
                             $view : $drupal_view->display_handler->options['style_options']['title'];
  }
  return $views;
}

/**
 * Function retrieves the pertinent information about a view in the system
 *
 * @param $view_name
 *  Name of the view we wish information about
 * @param $display_name
 *  Name of the display in the view we wish information about
 */
function mobile_services_views_information($view_name = NULL, $display_name = 'default') {
  return _mobile_services_views_describe($view_name, $display_name);
}

/**
 * Function to execute the view and return the information to us
 *
 * @param $view_name
 *  Name of the view we wish to read / execute
 * @param $display_name
 *  Name of the display we wish to read / execute
 */
function mobile_services_views_read($view_name = NULL, $display_name = 'default', $args = array(), $offset = 0, $limit = 0) {
  $view = views_get_view($view_name);
  if (!$view) {
    return array();
  }
  // Retrieve view display name and then set it and build the view so we have all information
  $display_id = _mobile_services_get_view_display_id_by_name($view, $display_name);
  // Put all arguments and then execute
  $view->set_display($display_id);
  $view->set_arguments($args, FALSE);
  $view->set_offset($offset);
  $view->set_items_per_page($limit);
  $view->execute();

  return $view->result;
}


/**
 * Retrieving the description for the view we want to retrieve
 *
 * @param $view_name
 *  Name of the view we wish information about
 * @param $display_name
 *  Name of the display in the view we wish information about
 */
function _mobile_services_views_describe($view_name = NULL, $display_name = NULL) {
  if (is_null($view_name) || is_null($display_name)) {
    return array();
  }

  $view = views_get_view($view_name);

  if (!$view) {
    return array();
  }
  // Retrieve view display name and then set it and build the view so we have all information
  $display_id = _mobile_services_get_view_display_id_by_name($view, $display_name);
  $view->set_display($display_id);
  $view->build();

  // Setting the results array with pertinent information for the view that will be needed
  $result = array('fields' => array(), 'fields_config' => array(), 'options' => array());
  $result['options']['title'] = $view->display_handler->options['style_options']['title'];
  $result['options']['viewtype'] = $view->display_handler->options['style_options']['viewtype'];
  $result['options']['primary'] = $view->display_handler->options['style_options']['primary'];
  $result['options']['grouping_field'] = $view->display_handler->options['style_options']['grouping_field'];
  // Getting the list of fields and any pertinent infromation regarding them
  foreach (array_keys($view->field) as $field) {
    $result['fields'][] = $field;
    $result['fields_config'][$field] = array(
      'label' => $view->field[$field]->options['label'],
      'type' => _mobile_services_guess_field_type($view->field[$field]),
      'excluded' => !empty($view->field[$field]->options['exclude']),
      'alias' => $view->field[$field]->field_alias,
    );
    // Determine whether we have a datatype saved in our view fields information table for this particular field
    $datatype = db_result(db_query("SELECT field_value FROM {mobile_services_view_fields_info} WHERE view = '%s' AND display_id = '%s' AND property = '%s' AND field_name = '%s'", $view->name, $display_id, 'data_type', $field));
    if ($datatype) {
      // Add the datatype specified in the view field configuration if one exists
      $result['fields_config'][$field]['data_type'] = $datatype;
    }
  }

  return $result;
}

/**
 * Function for guessing the type of field in the view
 *
 * @param $views_field
 *  Field we wish to know the type of
 */
function _mobile_services_guess_field_type($views_field) {
  if (isset($views_field->content_field) && isset($views_field->content_field['type'])) {
    return _mobile_services_guess_cck_type($views_field->content_field['type']);
  }
  if (isset($views_field->table) && isset($views_field->field)) {
    if ($views_field->table == 'node_revisions' && $views_field->field == 'body') {
      return 'string';
    } elseif ($views_field->table == 'node' && $views_field->field == 'nid') {
      return 'int';
    } elseif ($views_field->table == 'node' && $views_field->field == 'title') {
      return 'string';
    }
  }

  return 'string';
}

/**
 * Function guesses the cck type for the field type we received
 *
 * @param $type
 *  Type we wish to know information about
 */
function _mobile_services_guess_cck_type($type) {
  if ($type == 'number_integer') {
    return 'int';
  }
  return $type;
}

/**
 * Utility function to retrieve the display id by the view display name
 *
 * @param $view_name
 *  Name of the view we want the display id for
 * @param $display_name
 *  Name of the display we want the display id for
 */
function _mobile_services_get_view_display_id_by_name($view, $display_name) {
  $display_id = NULL;
  foreach ($view->display as $id => $display) {
    if ($display->display_title == $display_name) {
      $display_id = $id;
      break;
    }
  }
  return is_null($display_id) ? $display_name : $display_id;
}
