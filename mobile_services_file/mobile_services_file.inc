<?php

/**
 * Saves a file to the bitcache and creates a file node.
 *
 * @param $edit
 *   An array of the node fields' values, just like created on node edit form.
 * @param $data
 *   Bitstream's contents.
 * @param $id
 *   Bitstream's identifier (SHA-1 fingerprint).
 *
 * @return
 *   A file node ID or FALSE.
 */
function mobile_services_file_save($edit, $data, $node_id = NULL, $id = NULL) {
  // Load node service module include since we use node_service_save
  module_load_include('inc', 'node_service', 'node_service');
  // Set the bitcache repository we are using
  bitcache_use_repository(FILE_BITCACHE_REPOSITORY);
  // Save the file too bitcache
  $hash = bitcache_put($id, $data);
  // Remove lock from the bitcache repository
  bitcache_use_repository();
  // Return -1 if the file failed
  if (!$hash) {
    return (int)-1;
  }
  $filecount = 0;
  // Load the node we are attaching the file too
  $node = node_load($node_id);
  if ($node && is_object($node)) {
    $edit = (array)$edit;
    $edit['type'] = 'file';
    $edit['file'] = $hash;
    $edit['taxonomy'] = $node->taxonomy;
    $edit['og_groups'] = $node->og_groups;
    $edit['og_groups_both'] = $node->og_groups_both;
    $fid = node_service_save($edit);
    // Verify if the node_id was passed and if so attach the file too it
    if ($fid && is_numeric($fid)) {
      $node->files = array();
      $file_node = node_load($fid);
      if ($file_node && is_object($file_node)) {
        $file_node->file->list = 1;
        $file_node->file->reuse = TRUE;
        $file_node->file->name = $file_node->title;
        $node->files['s_'. $filecount] = (array)$file_node->file;
        node_save($node);
      }
    }
  }
  return isset($fid) ? (int)$fid : (int)-1;
}

