
HOOKS
-----

mobile_services_field_value: hook to determine the value for a field if all other ways fail

 * $key - name of the field we wish the value for
 * $value - structure the value currently is in (we want a string returned)

