<?php

class mobile_services_display_plugin extends views_plugin_display {
  function get_style_type() { return 'mobile'; }

  /**
   * Feeds do not go through the normal page theming mechanism. Instead, they
   * go through their own little theme function and then return NULL so that
   * Drupal believes that the page has already rendered itself...which it has.
   */
  function execute() {
    $output = $this->view->render();
    if (empty($output)) {
      return drupal_not_found();
    }
    print $output;
  }

  function preview() {
    return '<pre>' . print_r($this->view->render()) . '</pre>';
  }

  /**
   * Instead of going through the standard views_view.tpl.php, delegate this
   * to the style handler.
   */
  function render() {
    return $this->view->style_plugin->render($this->view->result);
  }

  function option_definition() {
    $options = parent::option_definition();

    // Overrides for standard stuff:
    $options['style_plugin']['default'] = 'mobile_services_row';
    $options['style_options']['default']  = array();
    $options['row_plugin']['default'] = 'mobile_services_row';
    $options['row_options']['default'] = array();

    return $options;
  }
}
