<?php

class mobile_services_style_plugin extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    $options['viewtype'] = array('default' => '');
    $options['title'] = array('default' => '');
    $options['strip_html'] = array('default' => true);
    $options['grouping_field'] = NULL;
    $options['primary'] = NULL;
    return $options;
  }

  function render() {
    $rows = array();
    foreach ($this->view->result as $row) {
      $rows[] = $this->row_plugin->render($row, $this->options['strip_html']);
    }

    $result = array(
      'rows' => $rows
    );

    if (!empty($this->view->total_rows)) {
      $result['total'] = $this->view->total_rows + (empty($this->view->pager['offset']) ? 0 : $this->view->pager['offset']);
    }

    return $result;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $fields = array();
    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      $fields[$field] = $handler->ui_name();
    }

    unset($form['grouping']);

    $form['viewtype'] = array(
      '#type' => 'select',
      '#title' => t('View Type'),
      '#description' => t('Type of view whether panel, regular, etc'),
      '#options' => array('list' => 'list', 'panel' => 'panel'),
      '#default_value' => $this->options['viewtype'],
    );

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#description' => t('The label for the mobile device tab.'),
      '#size' => 25,
      '#default_value' => $this->options['title'],
    );

    $form['strip_html'] = array(
      '#type' => 'checkbox',
      '#title' => t('Strip HTML'),
      '#description' => t('Strip html from the output'),
      '#default_value' => $this->options['strip_html'],
    );

    $form['grouping_field'] = array(
      '#type' => 'radios',
      '#title' => t('Field to use for grouping'),
      '#description' => t('Field to group results on if you wish to have the fields grouped i.e. date, type etc'),
      '#options' => $fields,
      '#default_value' => $this->options['grouping_field'],
    );

    $form['primary'] = array(
      '#type' => 'radios',
      '#title' => t('Primary field in this view'),
      '#description' => t('Field that is the unique identifier for this particular object'),
      '#options' => $fields,
      '#default_value' => $this->options['primary'],
    );
  }
}
