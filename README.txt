
The mobile services module is a drupal proxy module that will provide
all pertinent information to mobile clients regarding the views, data 
and content types available for display.

INSTALLATION
------------

Install and enable the Mobile Services Drupal module as you would any Drupal module.

Configure the module at Administer > Site Configuration > Mobile Services

DEPENDENCIES
------------

Module is written purely for Drupal 6.x core.

CREDITS
-------
Developed and maintained by Darren Ferguson <http://www.openbandlabs.com/crew/darrenferguson>
Development sponsored by OpenBand, a subsidiary of M.C.Dean, Inc. <http://www.openbandlabs.com/>
