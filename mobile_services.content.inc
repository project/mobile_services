<?php

/**
 * Function returns a list of content types supported for the mobile application
 */
function mobile_services_content_types_list() {
  $content_types = array();
  $ctypes = node_get_types('names');
  $allowed_content_types = variable_get('mobile_services_allowed_content_types', array());
  foreach ($allowed_content_types as $name => $data) {
    $content_type_name = isset($ctypes[$name]) ? $ctypes[$name] : $name;
    $type_info = array('content_type' => $name, 'content_type_name' => $content_type_name);
    $content_types[] = $type_info;
  }
  return $content_types;
}

/**
 * Function returns a description of content types supported for the mobile application
 */
function mobile_services_content_types_describe($type = NULL) {
  $content_types = array();
  $ctypes = node_get_types('names');
  $allowed_content_types = variable_get('mobile_services_allowed_content_types', array());
  foreach ($allowed_content_types as $name => $data) {
    if ($type && strcmp($name, $type)) { continue; }
    $form_data = $field_info = $data = array();
    $form_fields = _mobile_services_forms_describe('node', $name);
    $datafields = db_query("SELECT * FROM {mobile_services_content_fields_info} WHERE content_type = '%s'", $name);
    while ($row = db_fetch_object($datafields)) {
      $field_info[$row->field_name] = $row->field_name;
      $form_data[$row->field_name] = array();
      // Doing some checks based on special case fields in the form
      if (in_array($row->field_name, array('body_field', 'taxonomy', 'og_nodeapi'))) {
        $form_data[$row->field_name] = _mobile_services_content_types_special_field($row->field_name, $form_fields);
      }
      else {
        if (isset($form_fields[$row->field_name])) {
          $form_data[$row->field_name] = $form_fields[$row->field_name];
        }
        else {
          $info = array();
          _mobile_services_content_cck_type_search($form_fields, $row->field_name, $info);
          if (is_array($info) && !empty($info)) {
            $info = _mobile_services_content_cck_field_special_processing($info);
            $form_data[$row->field_name] = $info;
          }
        }
      }
    }
    // Retrieveing the pertinent information we are looking for from the system
    $data['content_type_name'] = isset($ctypes[$name]) ? $ctypes[$name] : $name;
    $data['fields'] = $field_info;
    $data['form_fields'] = $form_data;
    if ($type) {
      $content_types = $data;
    }
    else {
      $content_types[$name] = $data;
    }
  }
  return $content_types;
}

/**
 * Function to retrieve the relevant information
 */
function _mobile_services_content_cck_type_search($form_fields, $field_name, &$info) {
  foreach ($form_fields as $key => $value) {
    if (is_array($value) && strcmp($key, $field_name)) {
      _mobile_services_content_cck_type_search($value, $field_name, $info);
    }
    else {
      if (strcmp($key, $field_name) == 0) {
        $info = $value;
        break;
      }
    }
  }
}

/**
 * Function to do specific handling of CCK fields since we might not have all fields we require
 */
function _mobile_services_content_cck_field_special_processing($field_info = array()) {
  $info = $field_info;
  if (isset($field_info['#type']) && !empty($field_info['#type'])) {
    if (isset($field_info['value']) && is_array($field_info['value'])) {
      $info = $field_info['value'];
      // Doing some checking on the options types since they don't seem to have the correct naming
      // Would be fine if we were HTTP posting the info back as a form but not in this case using services
      if (in_array($field_info['#type'], array('optionwidgets_select', 'optionwidgets_buttons', 'optionwidgets_buttons'))) {
        if (strpos($info['#name'], '[0]') === FALSE) {
          $info['#name'] = str_replace('[value]', '[0][value]', $info['#name']);
        }
      }
    }
  }
  else {
    if (isset($field_info[0]) && is_array($field_info[0])) {
      if (isset($field_info[0]['value']) && is_array($field_info[0]['value'])) {
        $info = $field_info[0]['value'];
      }
    }
  }
  return $info;
}

/**
 * Specific code to handle special fields that are not what we normally have
 */
function _mobile_services_content_types_special_field($field_name = NULL, $form_fields = NULL) {
  $field_info = array();
  if (!is_null($field_name)) {
    switch ($field_name) {
      case 'body_field':
        $field_info = $form_fields['body_field']['body'];
        break;
      case 'og_nodeapi':
        $field_info = $form_fields['og_nodeapi']['visible']['og_groups'];
        break;
      case 'taxonomy':
        foreach ($form_fields['taxonomy']['tags'] as $tagid => $tagvalue) { break; }
        $field_info = $tagvalue;
        break;
    }
  }
  return $field_info;
}
