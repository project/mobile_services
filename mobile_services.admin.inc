<?php
// $Id

/**
 * Function to configure the content types that the mobile_services app will support
 */
function mobile_services_admin_content_types(&$form_state) {
  $content_types = $form = array();
  $allowed_content_types = variable_get('mobile_services_allowed_content_types', array());
  $content_types = node_get_types('names');
  if (is_array($allowed_content_types) && !empty($allowed_content_types)) {
    $rows = array();
    $header = array('content_type' => t('Content Type'), 'link' => t('Operation'));
    foreach ($allowed_content_types as $content_type => $fields) {
      $edit = l(t('Edit'), 'admin/settings/mobile_services/content/edit/'. $content_type);
      $delete = l(t('Delete'), 'admin/settings/mobile_services/content/delete/'. $content_type);
      $link = $edit .' | '. $delete;

      $rows[] = array('content_type' => $content_types[$content_type], 'link' => $link);
      unset($content_types[$content_type]);
    }
    // Only display this section if the allowed views are not empty
    if (!empty($rows)) {
      $form['allowed_content_types'] = array('#value' => theme('table', $header, $rows));
    }
  }

  $form['content_type'] = array(
    '#type' => 'select',
    '#title' => t('Content Type'),
    '#options' => $content_types,
    '#description' => t('Available Content Types'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $form['#theme'] = 'mobile_services_content_types_allowed_content_types_form';
  return $form;
}

/**
 * Submit handler for the views form
 */
function mobile_services_admin_content_types_submit(&$form, &$form_state) {
  $allowed_content_types = variable_get('mobile_services_allowed_content_types', array());
  $content_type = $form_state['values']['content_type'];
  if (isset($allowed_content_types[$content_type])) {
    drupal_set_message(t('Value is already mapped in the system, you must remove the existing mapping before you can re-map the value'), 'error');
  }
  else {
    $allowed_content_types[$content_type] = array('content_type' => $content_type);
    variable_set('mobile_services_allowed_content_types', $allowed_content_types);
    drupal_set_message(t('Content Type has been successfully mapped'));
  }
  $form_state['redirect'] = 'admin/settings/mobile_services/content';
}

/**
 * Removing the existing content type from the system
 */
function mobile_services_admin_content_types_delete($content_type = NULL) {
  $allowed_content_types = variable_get('mobile_services_allowed_content_types', array());
  if (isset($allowed_content_types[$content_type]) && !empty($allowed_content_types[$content_type])) {
    // Removing all of the field mappings for the content type from the database
    db_query("DELETE FROM {mobile_services_content_fields_info} WHERE content_type = '%s'", $content_type);
    unset($allowed_content_types[$content_type]); // removing the mapping completely
    variable_set('mobile_services_allowed_content_types', $allowed_content_types);
    drupal_set_message(t('!content_type removed from the configured mobile content types', array('!content_type' => $content_type)));
  }
  else {
    drupal_set_message(t('Content Type received is not one of the configured mobile content types'), 'error');
  }
  drupal_goto('admin/settings/mobile_services/content');
}

/**
 * Edit the existing content type in the system
 */
function mobile_services_admin_content_types_edit(&$form_state, $content_type = NULL) {
  $allowed_content_types = variable_get('mobile_services_allowed_content_types', array());
  $content_type = $content_type ? $content_type : $form_state['storage']['content_type'];
  if (empty($content_type)) {
    drupal_set_message(t('No content type was received by the application please retry your selection'), 'error');
    drupal_goto('admin/settings/mobile_services/content');
  }
  if (!isset($allowed_content_types[$content_type])) {
    drupal_set_message(t('Content Type is not one of the configured mobile content types'), 'error');
    drupal_goto('admin/settings/mobile_services/content');
  }
  $rows = array();
  $header = array('field_name' => t('Field Name'), 'link' => t('Operation'));
  $options = mobile_services_content_type_get_fields($content_type);
  $result = db_query("SELECT field_id, field_name FROM {mobile_services_content_fields_info} WHERE content_type = '%s' ORDER BY screen_position DESC, precedence ASC", $content_type);
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      'field_name' => $options[$row->field_name],
      'link' => l(t('Edit'), 'admin/settings/mobile_services/content/field/edit/'. $row->field_id),
    );
    unset($options[$row->field_name]);
  }
  // Display the rows if we actually have some saved in the system
  if (!empty($rows)) {
    $form['mappings'] = array('#value' => theme('table', $header, $rows));
  }
  // Saving the view name for use during form submission
  $form_state['storage']['content_type'] = $content_type;
  $form['content_type'] = array('#type' => 'hidden', '#value' => $content_type);
  $form['fields'] = array(
    '#type' => 'select',
    '#title' => t('Fields'),
    '#options' => $options,
    '#description' => t('Fields that are associated with the content type in the system'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Add'));
  $form['#theme'] = 'mobile_services_content_types_field_mappings_form';
  return $form;
}

/**
 * Edit the existing view in the system
 */
function mobile_services_admin_content_types_edit_submit(&$form, &$form_state) {
  if (isset($form_state['values']['content_type']) && !empty($form_state['values']['content_type']) &&
      isset($form_state['values']['fields']) && !empty($form_state['values']['fields'])) {
    $record = array(
      'content_type' => $form_state['values']['content_type'],
      'field_name' => $form_state['values']['fields'],
    );
    // Create a new record in the system for the content type field
    drupal_write_record('mobile_services_content_fields_info', $record);
    drupal_set_message(t('!content_type has been added to the list of allowed Mobile Content Types', array('!content_type' => $form_state['values']['content_type'])));
  }
  drupal_goto('admin/settings/mobile_services/content/edit/'. $form_state['values']['content_type']);
}

/**
 * Function to configure the pertinent information for a field in the content type
 */
function mobile_services_admin_content_types_edit_field(&$form_state, $field_id = NULL) {
  // Building the form and retrieving already saved values for the field and content type combination
  $form = array();
  $form['field_id'] = array('#type' => 'hidden', '#value' => $field_id);
  $field_values = db_fetch_array(db_query("SELECT * FROM {mobile_services_content_fields_info} WHERE field_id = %d", $field_id));
  $form['field_name'] = array('#type' => 'hidden', '#value' => $field_values['field_name']);
  $form['content_type'] = array('#type' => 'hidden', '#value' => $field_values['content_type']);
  // Position of where the information will go on the mobile application screen
  $form['screen_position'] = array(
    '#type' => 'select',
    '#title' => t('Screen Position'),
    '#options' => array('body' => t('Body'), 'head' => t('Head')),
    '#default_value' => isset($field_values['screen_position']) ? $field_values['screen_position'] : 'head',
  );
  $form['precedence'] = array(
    '#type' => 'textfield',
    '#title' => t('Field Precedence'),
    '#description' => t('Where the field will precide in regard to other fields in the screen area'),
    '#default_value' => isset($field_values['precedence']) ? $field_values['precedence'] : '0',
  );
  $form['hide_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide from Display'),
    '#description' => t('Check if this field will be hidden from the users display'),
    '#default_value' => isset($field_values['hide_display']) ? $field_values['hide_display'] : 0,
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Field Label'),
    '#description' => t('Label for the field if it requires one for viewing purposes'),
    '#default_value' => isset($field_values['label']) ? $field_values['label'] : '',
  );
  $form['label_position'] = array(
    '#type' => 'select',
    '#title' => t('Label Position'),
    '#description' => t('Determine where the label will sit on the screen in relation to the field'),
    '#options' => array('above' => t('Above'), 'left' => t('Left')),
    '#default_value' => isset($field_values['label_position']) ? $field_values['label_position'] : 'left',
  );
  $form['hide_label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide Label From Display'),
    '#description' => t('Check if you wish the field info to be displayed but not the label for the field'),
    '#default_value' => isset($field_values['hide_label']) ? $field_values['hide_label'] : 0,
  );
  $form['datatype'] = array(
    '#type' => 'radios',
    '#title' => t('Field Data Type'),
    '#description' => t('Data type helper for the field so we can guage the correct transformation functions if necessary to display it'),
    '#options' => array('date' => t('Date'), 'int' => t('Integer'), 'string' => t('String')),
    '#default_value' => isset($field_values['datatype']) ? $field_values['datatype'] : 'string',
  );
  $form['highlight'] = array(
    '#type' => 'checkbox',
    '#title' => t('Highlight Field'),
    '#description' => t('If checked the field will be highlighted on the phone from other fields'),
    '#default_value' => isset($field_values['highlight']) ? $field_values['highlight'] : 0,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Update'));
  return $form;
}

/**
 * Form submission handler for the admin content types edit field
 */
function mobile_services_admin_content_types_edit_field_submit(&$form, &$form_state) {
  $record = array();
  // Loop through each of the values and add them too the system
  foreach (array('field_id', 'content_type', 'field_name', 'screen_position', 'precedence', 'hide_display', 'label', 'label_position', 'hide_label', 'datatype', 'highlight') as $value) {
    $record[$value] = $form_state['values'][$value];
  }
  // Update an existing record in the system for the content type field
  drupal_write_record('mobile_services_content_fields_info', $record, array('field_id'));
  drupal_set_message(t('!field_name field\'s configuration has been updated in the system', array('!field_name' => $form_state['values']['field_name'])));
  drupal_goto('admin/settings/mobile_services/content/edit/'. $form_state['values']['content_type']);
}

/**
 * Function to configure the views that the mobile_services app will support
 */
function mobile_services_admin_views(&$form_state) {
  $available_views = $form = array();
  $views = views_get_all_views();
  $allowed_views = variable_get('mobile_services_allowed_views', array());
  foreach ($views as $view => $object) {
    // Only show enabled views
    if (empty($object->disabled)) {
      $available_views[$view] = $view;
    }
  }
  // Go through the allowed views and display them onto the screen
  if (is_array($allowed_views) && !empty($allowed_views)) {
    $rows = array();
    $header = array('view' => t('View'), 'link' => t('Operation'));
    foreach ($allowed_views as $view_name => $view_object) {
      $rows[] = array('view' => $view_name, 'link' => l(t('Edit'), 'admin/settings/mobile_services/views/edit/'. $view_name));
      unset($available_views[$view_name]);
    }
    // Only display this section if the allowed views are not empty
    if (!empty($rows)) {
      $form['allowed_views'] = array('#value' => theme('table', $header, $rows));
    }
  }

  $form['views'] = array(
    '#type' => 'select',
    '#title' => t('Views'),
    '#options' => $available_views,
    '#description' => t('Available Views'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $form['#theme'] = 'mobile_services_views_allowed_views_form';
  return $form;
}

/**
 * Submit handler for the views form
 */
function mobile_services_admin_views_submit(&$form, &$form_state) {
  $allowed_views = variable_get('mobile_services_allowed_views', array());
  $view = $form_state['values']['views'];
  if (isset($allowed_views[$view])) {
    drupal_set_message(t('Value is already mapped in the system, you must remove the existing mapping before you can re-map the value'), 'error');
  }
  else {
    $allowed_views[$view] = array('view' => $view, 'display_id' => 'default', 'display_name' => 'Defaults');
    variable_set('mobile_services_allowed_views', $allowed_views);
    drupal_set_message(t('View has been successfully mapped'));
  }
  $form_state['redirect'] = 'admin/settings/mobile_services/views';
}

/**
 * Edit the existing view in the system
 */
function mobile_services_admin_views_edit(&$form_state, $view_name = NULL) {
  $allowed_views = variable_get('mobile_services_allowed_views', array());
  $view_name = $view_name ? $view_name : $form_state['storage']['view_name'];
  if (empty($view_name)) {
    drupal_set_message(t('No view name was received by the application please retry your selection'), 'error');
    drupal_goto('admin/settings/mobile_services/views');
  }
  if (!isset($allowed_views[$view_name])) {
    drupal_set_message(t('View is not one of the configured mobile views'), 'error');
    drupal_goto('admin/settings/mobile_services/views');
  }
  // Saving the view name for use during form submission
  $form_state['storage']['view_name'] = $view_name;
  $view = views_get_view($view_name);
  if (!isset($view->display) || !is_array($view->display)) {
    $view->init_display();
  }
  $view_displays = array();
  foreach (array_keys($view->display) as $display_id) {
    $view_displays[$display_id] = $view->display[$display_id]->display_title;
  }
  $form_state['storage']['view_displays'] = $view_displays;
  $form['view_name'] = array(
    '#type' => 'hidden',
    '#value' => $view_name,
  );
  $form['display_id'] = array(
    '#type' => 'select',
    '#options' => $view_displays,
    '#title' => t('View Display'),
    '#default_value' => $allowed_views[$view_name]['display_id'],
  );
  $form['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['#theme'] = 'mobile_services_views_edit_form';
  return $form;
}

/**
 * Submit handler for the admin views edit form
 */
function mobile_services_admin_views_edit_submit(&$form, &$form_state) {
  $view_name = $form_state['values']['view_name'];
  $allowed_views = variable_get('mobile_services_allowed_views', array());
  // Delete the view from the list allowed by the mobile platform
  switch ($form_state['values']['op']) {
    case 'Delete':
      if (isset($allowed_views[$view_name])) {
        unset($allowed_views[$view_name]);
        drupal_set_message(t('View has been removed from the mobile allowed views'));
      }
      break;
    case 'Update':
      $view_displays = $form_state['storage']['view_displays'];
      $display_id = $form_state['values']['display_id'];
      $allowed_views[$view_name]['display_id'] = $display_id;
      $allowed_views[$view_name]['display_name'] = $view_displays[$display_id];
      break;
  }
  // Re-saving the allowed views for future usage
  variable_set('mobile_services_allowed_views', $allowed_views);
  drupal_goto('admin/settings/mobile_services/views');
}
